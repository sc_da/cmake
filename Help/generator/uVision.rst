uVision
--------

Generates ARM/Keil uVision Multi Project Workspaces (experimental, work-in-progress).

The generated Project files are generated from a template uVision5 Project File.
The template is provided via CMake Variable ''CMAKE_UVISION5_TEMPLATE''.

.. note::
  This generator is deemed experimental as of CMake |release|
  and is still a work in progress.  Future versions of CMake
  may make breaking changes as the generator matures.
