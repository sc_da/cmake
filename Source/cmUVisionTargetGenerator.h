/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt or https://cmake.org/licensing for details.  */
#ifndef cmUVisionTargetGenerator_h
#define cmUVisionTargetGenerator_h

#include "cmTarget.h"
#include <map>

class cmCustomCommand;
class cmGeneratedFileStream;
class cmGeneratorTarget;
class cmGlobalUVisionGenerator;
class cmLocalUVisionGenerator;
class cmMakefile;
class cmSourceFile;

class cmUVisionTargetGenerator
{
public:
  enum Types
  {
    PROJECT = 0,
    UTILITY = 1,

    LIBRARY = 10,
    SUBPROJECT = 11,
    PROGRAM = 12,

    REFERENCE = -1,
  };

  cmUVisionTargetGenerator(cmGeneratorTarget* target);

  virtual ~cmUVisionTargetGenerator();

  virtual void Generate();

private:
  cmGlobalUVisionGenerator* GetGlobalGenerator() const;

  void GenerateTarget();

  std::string GetDefines(const std::string& language,
                         std::string const& config);

private:
  std::string getTemplateFileContent();

  cmGeneratorTarget* GeneratorTarget;
  cmLocalUVisionGenerator* LocalGenerator;
  cmMakefile* Makefile;
  std::map<std::string, std::string> FlagsByLanguage;
  std::map<std::string, std::string> DefinesByLanguage;

  std::string TargetNameReal;
  std::string const Name;
  std::string ConfigName; /* CMAKE_BUILD_TYPE */
};

#endif // ! cmUVisionTargetGenerator_h
