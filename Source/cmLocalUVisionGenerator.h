/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt or https://cmake.org/licensing for details.  */
#ifndef cmLocalUVisionGenerator_h
#define cmLocalUVisionGenerator_h

#include "cmLocalGenerator.h"

class cmGeneratedFileStream;

/** \class cmLocalGhsMultiGenerator
 * \brief Write Green Hills MULTI project files.
 *
 * cmLocalGhsMultiGenerator produces a set of .gpj
 * file for each target in its mirrored directory.
 */
class cmLocalUVisionGenerator : public cmLocalGenerator
{
public:
  cmLocalUVisionGenerator(cmGlobalGenerator* gg, cmMakefile* mf);

  virtual ~cmLocalUVisionGenerator();

  /**
   * Generate the makefile for this directory.
   */
  virtual void Generate();

  std::string GetTargetDirectory(
    cmGeneratorTarget const* target) const override;

private:
  void GenerateTargetsDepthFirst(cmGeneratorTarget* target,
                                 std::vector<cmGeneratorTarget*>& remaining);
};

#endif
