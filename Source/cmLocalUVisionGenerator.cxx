/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt or https://cmake.org/licensing for details.  */
#include "cmLocalUVisionGenerator.h"

#include "cmGeneratedFileStream.h"
#include "cmGeneratorTarget.h"
#include "cmGlobalUVisionGenerator.h"
#include "cmMakefile.h"
#include "cmSourceFile.h"
#include "cmUVisionTargetGenerator.h"

cmLocalUVisionGenerator::cmLocalUVisionGenerator(cmGlobalGenerator* gg,
                                                 cmMakefile* mf)
  : cmLocalGenerator(gg, mf)
{
}

cmLocalUVisionGenerator::~cmLocalUVisionGenerator()
{
}

std::string cmLocalUVisionGenerator::GetTargetDirectory(
  cmGeneratorTarget const* target) const
{
  std::string dir;
  dir += target->GetName();
  dir += ".dir";
  return dir;
}

void cmLocalUVisionGenerator::GenerateTargetsDepthFirst(
  cmGeneratorTarget* target, std::vector<cmGeneratorTarget*>& remaining)
{
  // Find this target in the list of remaining targets.
  auto it = std::find(remaining.begin(), remaining.end(), target);
  if (it == remaining.end()) {
    // This target was already handled.
    return;
  }
  // Remove this target from the list of remaining targets because
  // we are handling it now.
  *it = nullptr;

  cmUVisionTargetGenerator tg(target);
  tg.Generate();
}

void cmLocalUVisionGenerator::Generate()
{
  std::vector<cmGeneratorTarget*> remaining = this->GetGeneratorTargets();
  for (auto& t : remaining) {
    if (t) {
      GenerateTargetsDepthFirst(t, remaining);
    }
  }
}
