# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

#Setup Keil uVision 5 multi projekt workspaces

#WARNING: Avoid using full qualified Compiler-Name (Keil does support 8051,C16x,.. not only ARM), the used comiler depends on Keil-Project-Settings

set(CMAKE_C_COMPILER "C:/Keil_v5/ARM/ARMCC/bin/armcc.exe")
set(CMAKE_C_COMPILER_ID_RUN TRUE)
set(CMAKE_C_COMPILER_ID "keilc")
set(CMAKE_C_COMPILER_FORCED TRUE)

set(CMAKE_CXX_COMPILER "C:/Keil_v5/ARM/ARMCC/bin/armcc.exe")
set(CMAKE_CXX_COMPILER_ID_RUN TRUE)
set(CMAKE_CXX_COMPILER_ID "keilc")
set(CMAKE_CXX_COMPILER_FORCED TRUE)

set(CMAKE_AR            "keilc_dummy")
set(CMAKE_LINKER        "keilc_dummy")
set(CMAKE_RANLIB        "keilc_dummy")

set(CMAKE_STATIC_LIBRARY_SUFFIX_C   ".lib")
set(CMAKE_STATIC_LIBRARY_SUFFIX_CXX ".lib")
set(CMAKE_STATIC_LIBRARY_PREFIX_C   "")
set(CMAKE_STATIC_LIBRARY_PREFIX_CXX "")
